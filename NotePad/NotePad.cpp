﻿// Notepad.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Notepad.h"
#include <Windows.h>
#include <commctrl.h>
#include <commdlg.h>

#include"resource.h"

#define MAX_LOADSTRING 100
#define IDC_EDIT 130
// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

void LoadFileToEdit();

void SaveTextFileFromEdit();

bool GetFileNameForSave();

void checksave();


// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.

	// Initialize global strings
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_NOTEPAD, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_NOTEPAD));

	MSG msg;

	// Main message loop:
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_NOTEPAD));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_BTNFACE + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_NOTEPAD);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_NOTEPAD));

	return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	HWND hWnd = CreateWindowW(szWindowClass, L"Untitled - MyNotepad", WS_OVERLAPPEDWINDOW | WS_EX_APPWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

HWND g_hwnd;
HWND g_hEdit;
HFONT g_hFont;
WCHAR curfile[MAX_PATH];
bool needsave = false;
bool isopened = false;

void LoadFileToEdit()
{
	curfile[0] = '/0';
	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = g_hwnd;
	ofn.lpstrFilter = L"Text Files(*.txt)\0*.txt\0All File(*.*)\0*.*\0";
	ofn.lpstrFile = curfile;
	ofn.nMaxFile = MAX_PATH;
	ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
	ofn.lpstrDefExt = L"txt";
	if (!GetOpenFileName(&ofn))
		return;
	HANDLE hFile;
	bool bsucces = false;
	hFile = CreateFile(curfile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
	if (hFile != INVALID_HANDLE_VALUE)
	{
		DWORD dwFileSize;
		dwFileSize = GetFileSize(hFile, NULL);
		if (dwFileSize != 0xFFFFFFFF)
		{
			LPSTR tempftext;
			tempftext = (char*)GlobalAlloc(GPTR, dwFileSize + 1);
			if (tempftext != NULL)
			{
				DWORD dwRead;
				if (ReadFile(hFile, tempftext, dwFileSize, &dwRead, NULL))
				{
					tempftext[dwFileSize] = 0;
					if (SetWindowText(g_hEdit, (LPCWSTR)tempftext))
						bsucces = true;
				}
				GlobalFree(tempftext);
			}
		}
		CloseHandle(hFile);
	}
	if (!bsucces)
	{
		MessageBox(g_hwnd, L"Không thể mở file!!", L"Error", MB_OK | MB_ICONERROR);
		return;
	}
	SetWindowText(g_hwnd, curfile);
	needsave = false;
	isopened = true;
}

void SaveTextFileFromEdit()
{
	HANDLE hFile;
	bool bsucces = false;
	//Lưu file
	hFile = CreateFile(curfile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	//Nếu đường dẫn chưa có sẵn => lấy chuỗi => tạo file mới => ghi chuỗi vào file mới
	if (hFile != INVALID_HANDLE_VALUE)
	{
		DWORD dwTextLength;
		dwTextLength = GetWindowTextLength(g_hEdit);
		LPSTR pszText;
		DWORD dwBufferSize = dwTextLength + 1;
		pszText = (char*)GlobalAlloc(GPTR, dwBufferSize);
		if (pszText != NULL)
		{
			if (GetWindowText(g_hEdit, (LPWSTR)pszText, dwBufferSize))
			{
				DWORD dwWritten;
				if (WriteFile(hFile, pszText, dwTextLength, &dwWritten, NULL))
					bsucces = true;
			}
			GlobalFree(pszText);
		}

		CloseHandle(hFile);
	}
	if (!bsucces)
	{
		MessageBox(g_hwnd, L"Không thể lưu file!!!", L"Error", MB_OK | MB_ICONERROR);
		return;
	}
	isopened = true;
	needsave = false;
}

bool GetFileNameForSave()
{
	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = g_hwnd;
	ofn.lpstrFilter = L"Text Files(*.txt)\0*.txt\0All File(*.*)\0*.*\0";
	ofn.lpstrFile = curfile;
	ofn.nMaxFile = MAX_PATH;
	ofn.Flags = OFN_EXPLORER | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT;
	ofn.lpstrDefExt = L"txt";
	if (!GetSaveFileName(&ofn))
		return false;
	return true;
}
//Hàm gợi ý save trước khi thoát
void checksave()
{
	if (needsave)
	{
		int res;
		res = MessageBox(g_hwnd, L"Trong file có thay đổi!!!\nBạn có muốn lưu?", L"Warning!!", MB_YESNOCANCEL | MB_ICONINFORMATION);
		if (res == IDCANCEL)
			return;
		if (GetFileNameForSave())
			SaveTextFileFromEdit();

	}
}
//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	LRESULT retval = S_OK;
	static HWND hwndEdit, hwnd;
	switch (message)
	{
	case WM_CREATE:
	{
		hwndEdit = CreateWindowEx(0, L"Edit", nullptr, WS_CHILD | WS_VISIBLE | ES_LEFT | ES_MULTILINE | WS_VSCROLL | ES_AUTOVSCROLL | WS_HSCROLL | ES_AUTOHSCROLL
			, 0, 0, CW_USEDEFAULT, CW_USEDEFAULT, hWnd, (HMENU)IDC_EDIT, (HINSTANCE)GetWindowLong(hWnd, GWLP_HINSTANCE), nullptr);
		if (hwndEdit == nullptr) retval = E_FAIL;

		HFONT hFont;
		hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
		SendMessage(hwndEdit, WM_SETFONT, (WPARAM)hFont, MAKELPARAM(FALSE, 0));

		g_hEdit = hwndEdit;
		g_hFont = hFont;

		RECT rcClient;
		GetClientRect(g_hwnd, &rcClient);
		SetWindowPos(g_hEdit, NULL, 0, 0, rcClient.right, rcClient.bottom, SWP_NOZORDER);
	}
	break;
	//Đạt lại size cho window
	case WM_SIZE:
	{
		MoveWindow(hwndEdit, 0, 0, LOWORD(lParam), HIWORD(lParam), TRUE);
	}
	break;
	
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		int wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case ID_FILE_NEW:
		{	
			int res = MessageBox(hwndEdit, L"Bạn có muốn save?", L"Warning", MB_YESNOCANCEL);
			if (res == IDCANCEL)
				break;
			if (res == IDYES)
				if (isopened)
					SaveTextFileFromEdit();
				else
					if (GetFileNameForSave())
						SaveTextFileFromEdit();
			if (res == IDNO)
			{
			}
			SendMessage(hwndEdit, EM_SETSEL, 0, -1); // chon het CTRL + A
			SendMessage(hwndEdit, WM_CHAR, 8, 0);// Xoa DELETE
		}
		break;
		case ID_FILE_OPEN:
		{
			checksave();
			LoadFileToEdit();
		}
		break;
		case ID_FILE_SAVE:
		{
			if (needsave)
			{
				if (isopened)
					SaveTextFileFromEdit();
				else if (GetFileNameForSave())
					SaveTextFileFromEdit();
			}
		}
		break;
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case ID_FILE_EXIT:
			DestroyWindow(hWnd);
			break;
		case ID_EDIT_CUT:
			SendMessage(g_hEdit, WM_CUT, 0, 0);
			break;
		case ID_EDIT_COPY:
			SendMessage(g_hEdit, WM_COPY, 0, 0);
			break;
		case ID_EDIT_PASTE:
			SendMessage(g_hEdit, WM_PASTE, 0, 0);
			break;
		case IDC_EDIT:
			switch (HIWORD(wParam))
			{
			case EN_CHANGE:
				needsave = true;
				break;
			}
			break;
		}
	}
	break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code that uses hdc here...
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
